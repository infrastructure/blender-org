<?php

/* Areas registration. */
if (function_exists('register_sidebar')) {
	/* Homepage when posts are used as index (e.g Code Blog). */
	register_sidebar(
		array(
			'name' => 'Posts Index Header',
			'id' => 'posts-index-header',
			'before_widget' => '<section class="cb-header">',
			'after_widget'  => '</section>',
		));
}


/* ACF */
function acf_overrides() {
	/* Save and load ACF settings in the parent theme's acf-json folder. */
	acf_update_setting('save_json', get_template_directory() . '/acf-json');
	acf_append_setting('load_json', get_template_directory() . '/acf-json');
}
add_action( 'plugins_loaded', 'acf_overrides' );


function remove_some_widgets(){
	unregister_sidebar('banner-widgets');
}
add_action('widgets_init', 'remove_some_widgets');

?>
