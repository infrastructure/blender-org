<?php

/**
 * Chart Bars Block Template.
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

// Create string based on the block name, stripping the 'acf/'' bit.
$className = $block['name'];
$className = explode('/', $className);
$className = $className[1];

// Create id attribute allowing for custom "anchor" value.
$id = $className . '-' . $block['id'];
if( !empty($block['anchor']) ) {
  $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
if( !empty($block['className']) ) {
  $className .= ' ' . $block['className'];
}

// Convert WP align values into Bootstrap float-left/right.
if( !empty($block['align']) ) {
  $className .= ' float-' . $block['align'];
}

// Load values and assign defaults.
$items       = get_field('items') ?: 'Edit block to add stats items.';
$chart_data  = get_field('chart_settings');
$chart_max   = $chart_data['chart_max'] ?: 100;
$chart_unit  = $chart_data['chart_unit'];
$chart_notes = $chart_data['chart_notes'];
$bar_colors  = get_field('bar_colors');

$is_time     = $chart_data['chart_is_time'];
$show_values = $chart_data['show_values'];

$show_ticks  = $chart_data['show_ticks'];
$ticks_steps = $chart_data['ticks_steps'];

/* Generic styling. */
$show_block       = get_field('show_block');
$background_color = get_field('background_color');
$text_color       = get_field('text_color');
$style            = get_field('style');
?>
<?php if (!isset($show_block) or $show_block): ?>
<div id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($className); ?>">
  <div class="chart-bars-legend">
    <ul>
    <?php
      /* Each stats item. */
      foreach ($bar_colors as $color_item_id => $color_item):
        $legend_label  = $color_item['bar_color_label'];
        $dot_grad_1    = $color_item['bar_color_1'] ?: 'currentColor';
        $dot_grad_2    = $color_item['bar_color_2'] ?: 'currentColor';
        $dot_grad      = "linear-gradient(45deg, " . $dot_grad_1 . ", " . $dot_grad_2 . ")";
    ?>
      <?php if ($legend_label): ?>
        <li>
          <span class="chart-bars-legend-indicator" style="background: <?=$dot_grad?>;"></span>
          <span><?=$legend_label?></span>
        </li>
      <?php endif; ?>
    <?php endforeach;?>
    </ul>
  </div>

  <ul>
    <?php
    /* Each stats item. */
    foreach ($items as $bar_index => $item):
      $item_caption = $item['item_caption'];
      $item_bars    = $item['item_bars'];
      ?>
      <li class="chart-bars-item">
        <div class="chart-bars-item-caption"><?=$item_caption?></div>
        <div class="chart-bars-bars">
          <?php
          /* Each bar for the current item. */
          foreach ($item_bars as $bar_index => $bar):
            $bar_color_id = $bar['bar_color_id']? ($bar['bar_color_id'] - 1) : 0;
            $bar_caption  = $bar_colors[$bar_color_id]['bar_color_label'];

            $bar_value    = min($bar['bar_value'], $chart_max); // Prevent overflow by capping value at $chart_max.
            $bar_percent  = round((($bar_value / $chart_max) * 100), 1);
            $bar_grad_1   = $bar_colors[$bar_color_id]['bar_color_1'] ?: 'currentColor';
            $bar_grad_2   = $bar_colors[$bar_color_id]['bar_color_2'] ?: 'currentColor';
            $bar_gradient = "linear-gradient(45deg, " . $bar_grad_1 . ", " . $bar_grad_2 . ")";

            $bar_value_time = ($is_time ? intval(date('i', $bar_value)) . date(":s", $bar_value) : $bar_value);
            $bar_value_time = ($is_time ? date("i:s", $bar_value) : $bar_value);
            ?>

            <?php /* Container for each bar, its background, and value indicator. */ ?>
            <div class="chart-bars-bar-container">
              <div
              class="chart-bars-bar <?=($show_values ? 'show-values': '')?>"
              style="width: <?=$bar_percent?>%; background: <?=$bar_gradient?>;"
              title="<?=$bar_value?> <?=$chart_unit?> <?=($bar_caption ? '(' . $bar_caption . ')' : '')?>">

                <?php if ($show_values): ?>
                  <span class="chart-bars-bar-value">
                    <span><?=$bar_value_time?></span>
                  </span>
                <?php endif; ?>
              </div>
              <div class="chart-bars-bar background"></div>
            </div>
          <?php endforeach; ?>
        </div>
      </li>
    <?php endforeach; ?>
  </ul>

  <?php if ($show_ticks): ?>
  <div class="chart-bars-ticks">
    <ul>
    <?php
      $ticks_current = 0;
      $ticks_max = $chart_max;

      while ($ticks_current <= $ticks_max) {
        echo '<li>' . $ticks_current . '</li>';

        $ticks_current += $ticks_steps;
      }

      ?>
    </ul>
  </div>
  <?php endif; ?>

  <div class="chart-bars-legend">
    <?php /* Chart notes. */ ?>

    <?php if ($chart_unit or $chart_notes): ?>
      <ul>
        <?php if ($chart_notes) { echo '<li><span>' . $chart_notes . '</span></li>'; }?>
        <?php if ($chart_unit)  { echo '<li><span>Unit: ' . $chart_unit . '</span></li>'; }?>
      </ul>
    <?php endif; ?>
  </div>
  <?php if ($style or $background_color or $text_color): ?>
    <style type="text/css">
      #<?=$id?> {
        background: <?=$background_color?>;
        color: <?=$text_color?>;
      }
      <?=($style ? $style : '')?>
    </style>
  <?php endif; ?>
</div>
<?php endif; ?>
