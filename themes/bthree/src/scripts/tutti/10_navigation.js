$(function() {
	// Disable bootstrap's menu transition
	$.support.transition = false;
});

/* Get the (correct) offsetTop of an element.
 * by https://medium.com/@alexcambose
*/
const getOffsetTop = element => {
  let offsetTop = 0;
  while(element) {
    offsetTop += element.offsetTop;
    element = element.offsetParent;
  }
  return offsetTop;
}

function getUrlAnchor() {
	return (document.URL.split('#').length > 1) ? document.URL.split('#')[1] : null;
}

/* Override the default 'scrollTo' by the browser to add an
 * offset and prevent the heading to be covered by the navbar.
*/
let headingAnchors = document.querySelectorAll('.is-heading-anchor');

function goToHeading(e, offsetExtra) {

	/* If we're clicking on an anchor get its ID. */
	let id = e;
	if (e && e.pointerType == 'mouse') {
		e.preventDefault();
		id = e.target.getAttribute('id');
	}

	/* If we're replying a comment, jump to it. */
	let url = window.location.href;
	if (url.indexOf("?replytocom") > -1) {
		id = 'comment-' +
		url.substring(
			url.indexOf("=") + 1,
			url.lastIndexOf("#")
			);
	}

	/* Replace the hash in the URL. */
	if (id) {
		history.replaceState(undefined, undefined, '#' + id);
	}

	/* Get the anchor if any, otherwise abort. */
	let anchor;
	anchor = document.getElementById(id);

	if (!anchor) {
		return;
	}

	let offsetTop = 0;
	offsetTop = getOffsetTop(anchor);

	/* If no specific offset is defined, make some room for the navbar. */
	if (offsetExtra === undefined) {
		offsetExtra = 100;
	}

	/* Jump! */
	window.scrollTo({
		top: offsetTop - offsetExtra,
		left: 0,
		behavior: 'smooth'
	});
}

/* Listen to clicks on headings. */
for (var i = 0; i < headingAnchors.length; i++) {
	headingAnchors[i].addEventListener('click', goToHeading);
}

/* Jump to headings on load (if there's any hash in the URL). */
if (getUrlAnchor()) {
	goToHeading(getUrlAnchor());
}
