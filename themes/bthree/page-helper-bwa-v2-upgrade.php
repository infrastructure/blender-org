<?php
$page_width = get_field('page_width');
get_header();
?>
<div class="container">
  <div class="row">
    <div class="col-12">
      <div class="box">
        <h1>Page Helper BWA v2 Upgrade</h1>
        <?php
        get_template_part('part-helper-bwa-wp-search-and-replace');
        ?>
      </div>
    </div>
  </div>
</div>
<?php
get_footer('sitemap');
get_footer();
?>
