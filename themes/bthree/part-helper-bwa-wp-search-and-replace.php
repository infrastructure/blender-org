<?php
/**
 * Include this template part with 'get_template_part' to use BWA's search and replace development utilities for WordPress.
 * Adjust the details if necessary.
 */

$wp_search_and_replace_path = get_template_directory() . '/assets_shared/dev-utils/wp-search-and-replace.php';

// Set path to the search and replace map file
if (isset($_GET['post']) && $_GET['post'] == 'true') {
  // Use the following map file for post-upgrade search and replace
  $map_file_name_path = get_template_directory() . '/assets_shared/dev-utils/search-and-replace-map-v2-upgrade-post-wp.txt';
} else {
  $map_file_name_path = get_template_directory() . '/assets_shared/dev-utils/search-and-replace-map-v2-upgrade-wp.txt';
}

echo "<p><strong>Using the following map file:</strong><br>" . $map_file_name_path . "</p><hr>";

include $wp_search_and_replace_path;

// Check if 'drymode' query string is present and equals 'true' to run in dry mode
if (isset($_GET['drymode']) && $_GET['drymode'] == 'true') {
  $dry_mode = true;
}

wp_search_and_replace($map_file_name_path, $dry_mode);
