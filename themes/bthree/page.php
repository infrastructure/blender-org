<?php

$page_width = get_field('page_width');
?>

<?php get_header(); ?>
<?php get_header('static'); ?>

<div class="<?=($page_width ? $page_width : 'container')?>">
	<?php while ( have_posts() ) : the_post(); ?>
		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
			<div class="entry-content">
				<?php the_content(); ?>
			</div>
		</article>
	<?php endwhile; ?>
</div>

<?php get_footer('sitemap'); ?>
<?php get_footer(); ?>
