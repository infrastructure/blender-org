<?php
if ( post_password_required() )
	return;
?>
<?php


function format_comment($comment, $args, $depth) {

	$GLOBALS['comment'] = $comment;

	$is_moderated  = ($comment->comment_approved == '0' ? ' is-moderated' : '');
	$is_op         = ($comment->user_id == get_the_author_meta('id') ? ' is-op' : '');
	$is_own        = ($comment->user_id == get_current_user_id() ? ' is-own' : '');
	$has_children  = ($comment->get_children() ? ' has-children' : '');

	$comment_email = get_comment_author_email($comment);
	$is_collab     = email_exists($comment_email) && str_ends_with($comment_email, '@blender.org');
	$is_moderator  = current_user_can('edit_posts');
	?>

	<li <?php comment_class(); ?> id="comment-<?php comment_ID() ?>">
		<div class="comment-body<?=$is_op?><?=$is_own?><?=$is_moderated?><?=$has_children?>">
			<ul class="comment-body-extra">
				<li class="author">
					<?php printf(__('%s'), get_comment_author_link()) ?>
					<?php if (is_user_logged_in() && $is_own): ?>
						<span class="badge badge-warning"><i class="i-user" title="Your comment"></i> You</span>
					<?php endif;?>

					<?php if ($is_op): ?>
						<span class="badge badge-info"><i class="i-mic" title="Comment by post author"></i> Author</span>
					<?php endif;?>

					<?php if ($is_collab): ?>
						<span class="badge badge-secondary">Collaborator</span>
					<?php endif;?>
				</li>
				<?php if ($is_moderated): ?>
				<li>
					<span class="badge badge-warning">Awaiting Moderation</span>
				</li>
				<?php endif; ?>
				<?php if($is_moderator): ?>
				<li>
					<a href="/wp-admin/comment.php?action=editcomment&c=<?=comment_ID()?>">Edit</a>
				</li>
				<?php endif; ?>
				<li>
					<a class="comment-permalink" title="<?=get_comment_time()?> on <?=get_comment_date("l F j, Y")?>" href="<?php echo htmlspecialchars ( get_comment_link( $comment->comment_ID ) ) ?>">
						<?php printf(__('%1$s'), get_comment_date(), get_comment_time()) ?>
					</a>
				</li>
			</ul>
			<?php comment_text(); ?>

			<ul class="comment-body-extra footer">
				<li><?php comment_reply_link(array_merge( $args, array('depth' => $depth, 'max_depth' => $args['max_depth']))) ?></li>
			</ul>

		</div>
	</li>

<?php } ?>

<div class="blog-comments">
	<?php if ( have_comments() ) : ?>
		<div id="comments" class="blog-comments-header">
			<span><?=(get_comments_number() > 1 ? get_comments_number() . ' comments' : 'One comment')?></span>
		</div>

		<ol class="blog-comments-list">
			<?php wp_list_comments('type=comment&callback=format_comment'); ?>
		</ol>

		<?php
			// Are there comments to navigate through?
			if ( get_comment_pages_count() > 1 && get_option( 'page_comments' ) ) :
		?>
		<nav class="navigation comment-navigation" role="navigation">
			<h1 class="screen-reader-text section-heading"><?php _e( 'Comment navigation', 'twentythirteen' ); ?></h1>
			<div class="nav-previous"><?php previous_comments_link( __( '&larr; Older Comments', 'twentythirteen' ) ); ?></div>
			<div class="nav-next"><?php next_comments_link( __( 'Newer Comments &rarr;', 'twentythirteen' ) ); ?></div>
		</nav><!-- .comment-navigation -->
		<?php endif; // Check for comment navigation ?>

		<?php if ( ! comments_open() && get_comments_number() ) : ?>
		<div class="no-comments mt-4 text-muted text-center">
			<p>
				In order to prevent spam, comments are closed <?php echo get_option('close_comments_days_old'); ?> days after the post is published.
				</br>Feel free to continue the conversation on <a href="https://devtalk.blender.org/?utm_medium=code-closed-comments">the forums</a>.
			</p>
		</div>
		<?php endif; ?>
	<?php endif; // have_comments() ?>

	<?php if (comments_open()): ?>
		<ol id="comment-reply-form" class="blog-comments-list reply-form">
			<li class="comment">
				<div class="comment-body">
					<?php comment_form(); ?>
				</div>
			</li>
		</ol>
	<?php endif; ?>
</div>
