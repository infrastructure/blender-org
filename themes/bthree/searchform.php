<form action="/" method="get" class="navbar-search">
	<input class="form-control" type="text" name="s" id="search" value="<?php the_search_query(); ?>" placeholder="Search..." />
	<button type="submit"><i class="i-search"></i></button>
</form>
