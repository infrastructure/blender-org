wp.domReady( () => {
	// Add custom styles.
	wp.blocks.registerBlockStyle( 'core/paragraph', {
		name: 'paragraph-center-large',
		label: 'Centered Large',
	});

	wp.blocks.registerBlockStyle( 'core/column', {
		name: 'box',
		label: 'Box',
	});
	wp.blocks.registerBlockStyle( 'core/columns', {
		name: 'box',
		label: 'Box',
	});
	wp.blocks.registerBlockStyle( 'core/columns', {
		name: 'wide',
		label: 'Wide',
	});
	wp.blocks.registerBlockStyle( 'core/group', {
		name: 'box',
		label: 'Box',
	});

	// Cover blocks.
	wp.blocks.registerBlockStyle( 'core/cover', {
		name: 'rounded',
		label: 'Rounded',
	});
	wp.blocks.registerBlockStyle( 'core/cover', {
		name: 'cover-center-full',
		label: 'Background Center Full Size',
	});
	wp.blocks.registerBlockStyle( 'core/cover', {
		name: 'cover-left-half',
		label: 'Background Left Half',
	});
	wp.blocks.registerBlockStyle( 'core/cover', {
		name: 'cover-right-half',
		label: 'Background Right Half',
	});

	// Massive-title is used for big headers in Release Logs.
	wp.blocks.registerBlockStyle( 'core/heading', {
		name: 'massive-title',
		label: 'Massive Title',
	});
	// Also add it to paragraphs, since they have extra options like colored background.
	wp.blocks.registerBlockStyle( 'core/paragraph', {
		name: 'massive-title',
		label: 'Massive Title',
	});

	wp.blocks.unregisterBlockStyle('core/image', 'rounded');

	// Buttons.
	wp.blocks.registerBlockStyle( 'core/button', {
		name: 'primary',
		label: 'Primary',
	});
	wp.blocks.registerBlockStyle( 'core/button', {
		name: 'accent',
		label: 'Accent',
	});
	wp.blocks.registerBlockStyle( 'core/button', {
		name: 'text',
		label: 'Text Only',
	});

	// Separator.
	wp.blocks.unregisterBlockStyle( 'core/separator', 'default' );
	wp.blocks.unregisterBlockStyle( 'core/separator', 'wide' );
	wp.blocks.unregisterBlockStyle( 'core/separator', 'dots' );

	/* Register it again so it can be set as default, since
		 apparently there seems to be no way of doing it yet:
		 https://github.com/WordPress/gutenberg/issues/42297
	*/
	wp.blocks.registerBlockStyle( 'core/separator', {
		name: 'dots',
		label: 'Dots',
		isDefault: true,
	});
});
