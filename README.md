# blender.org WordPress Theme

'bthree' is the WordPress theme of blender.org's main website. We invite web developers to help and provide fixes.

Reporting issues and pull requests are welcome in the [project page](https://projects.blender.org/infrastructure/blender-org).

## Installation

1. Install [WordPress](https://developer.wordpress.org/advanced-administration/before-install/development/) locally (use `blender.local` as URL)
2. Delete the `wp-content` folder
3. `git clone` this repository as a `wp-content` folder
4. `cd wp-content`
5. `git submodule init`
6. `git submodule update`
7. `npm install` to install dependencies.
8. `npm run copy` to copy fonts and icons from assets_shared into the assets folder.
9. `npm run build` to finally build all stylesheets and javascript.

### Development

Run `npm start` in the root of the repo, this will to open `blender.local:3000` in your browser and refresh automatically on changes.


## Required Plugins
- [ACF Pro](http://www.advancedcustomfields.com/)

## Installation Notes
### Docker
If you choose to install WordPress via Docker, the `volumes` section is needed to override `wp-config.php` and the entire `wp-content` folder.

Example `docker-compose.yaml`:
```
  wordpress:
    image: wordpress
    container_name:          wordpress
    restart: always
    ports:
      - 8080:80
    volumes:
      - /home/<local_install_path>/wordpress/wp-content:/var/www/html/wp-content
      - /home/<local_install_path>/wordpress/wp-config.php:/var/www/html/wp-config.php
```

Remember to set the user:group (so you can e.g. update plugins):

1. `docker exec -ti wordpress bash`
2. `chown -R www-data:www-data /var/www`

### Apache
If you choose to install WordPress using Apache, you might want to check these settings:

1. Enable short_code (in php.ini)  - this allows code to run with "<?" as well as <?php". The template and plugins use both formats.
2. Enable "rewrite_module" or "mod_rewrite" - this allow the links to work since the permalink format is different than the default.

## Production Deploy

The [`deploy.sh`](https://projects.blender.org/infrastructure/blender-org/src/branch/main/deploy.sh) script is designed to automate the deployment process of _blender.org_ and _code.blender.org_, with an option for a dry run to simulate the deployment. It ensures that deployments are made from the `production` branch and allows merging changes from the `main` branch into `production`.

### Usage Instructions

1. **Perform a dry run:**<br>
Make sure you're on the `main` branch. Before executing the actual deployment, it's recommended to perform a dry run to simulate the deployment process without making actual changes to either the `production` branch or the server. Dry run is on by default.
```
./deploy.sh blender.org
```
or
```
./deploy.sh code.blender.org
```
The dry run will simulate merging from `main` to `production` and check what file changes are going to be deployed if the deployment host is correctly specified, without making changes.

2. **Checkout `production`:**<br>
If the result of dry run looks correct, checkout the `production` branch.
```
git checkout production
```

3. **Merge `main` to `production`:**<br>
The deployment happens at the checked-out state of the `production` branch by default. If you want to deploy latest `main`, pass in the `--merge-production` flag when running the script.
```
./deploy.sh --merge-production blender.org
```
or
```
./deploy.sh --merge-production code.blender.org
```

4. **Run the script:**<br>
To perform an actual deployment, run the command with the `--live-run` flag.
```
./deploy.sh --live-run blender.org
```
or
```
./deploy.sh --live-run code.blender.org
```
Optionally you can combine steps 3. and 4. in a single run.
```
./deploy.sh --live-run --merge-production blender.org
```
or
```
./deploy.sh --live-run --merge-production code.blender.org
```

The script provides basic error handling if anything doesn't work as expected.
