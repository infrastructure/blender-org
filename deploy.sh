#!/bin/bash -e

# Initialize variables
DRY_RUN_FLAG="--dry-run"
MERGE_PRODUCTION_FLAG=false
DEPLOYHOST=""

# Set ROOT to present working directory
ROOT=$(pwd)

# Parse arguments for '--live-run' flag,  '--merge-production' flag and deploy host
for arg in "$@"; do
  case $arg in
    --live-run)
      DRY_RUN_FLAG=""
      ;;
    --merge-production)
      MERGE_PRODUCTION_FLAG=true
      ;;
    *blender.org*)
      DEPLOYHOST=$arg
      ;;
    *)
      ;;
  esac
done

# Check if DEPLOYHOST is set
if [ -z "$DEPLOYHOST" ]; then
    echo "Usage: $0 [--live-run] blender.org" >&2
    exit 1
fi

# Save the current branch
CURRENT_BRANCH=$(git rev-parse --abbrev-ref HEAD)

# Check if the --live-run flag is passed
if [ -z "$DRY_RUN_FLAG" ]; then
    echo "LIVE RUN: Proceeding with checking the 'production' branch checkout requirement."
    # Now check if the current branch is "production"
    if [[ $CURRENT_BRANCH != "production" ]]; then
        echo "You are not on the 'production' branch. Please checkout the 'production' branch to deploy." >&2
        exit 1
    fi
else
    echo "DRY RUN: Proceeding without checking the 'production' branch checkout requirement."
fi

echo

# Merge "main" into "production" with fast-forward only, conditionally based on 'dry-run' and 'merge-production' flags
if [ "$MERGE_PRODUCTION_FLAG" == true ]; then
    if [ -z "$DRY_RUN_FLAG" ]; then
        if ! git merge --ff-only main; then
            echo "Failed to merge 'main' into 'production'. Please resolve any conflicts and try again." >&2
            exit 1
        fi

        echo "Merging 'main' into 'production'..."
        echo
        echo "Merge successful."
    else
        echo "DRY RUN: Would merge 'main' into 'production'."
    fi
else
    echo "The '--merge-production' flag was not passed in. Skipping merge."
fi

echo

# Push changes to the production branch, conditionally based on dry-run
if [ -z "$DRY_RUN_FLAG" ]; then
    if ! git push origin production; then
      echo "Failed to push changes to 'production'. Please check your permissions and try again." >&2
      exit 1
    fi

    echo "Pushing changes to 'production'..."
    echo
    echo "Push successful."
else
    echo "DRY RUN: Would push changes to 'production'."
fi

echo
echo "*** GULPA GULPA ***"

npm install
npm run build

# Deployment logic
cd ${ROOT}

CONTENT="$ROOT/"
DEPLOY_FILTER_FILE="${ROOT}/deploy-filter.txt"

if [ ! -d "$CONTENT" ]; then
    echo "Unable to find dir $CONTENT"
    exit 1
fi

# Check if deploy filter file exists
if [ ! -f "$DEPLOY_FILTER_FILE" ]; then
    echo "Unable to find filter file $DEPLOY_FILTER_FILE"
    exit 1
fi

echo
echo "*** SYNCING $DEPLOYHOST ***"

# Deploy to blender.org with optional dry-run
if [[ "$DEPLOYHOST" == 'blender.org' ]]; then
    rsync $DRY_RUN_FLAG --checksum --human-readable -i --recursive --update --verbose --exclude-from="$DEPLOY_FILTER_FILE" $CONTENT blender.org:/data/www/vhosts/www.blender.org/www/wp-content/
elif [[ "$DEPLOYHOST" == 'code.blender.org' ]]; then
    # Deploy to code.blender.org with optional dry-run
    rsync $DRY_RUN_FLAG --checksum --human-readable -i --recursive --update --verbose --exclude-from="$DEPLOY_FILTER_FILE" $CONTENT blender.org:/data/www/vhosts/code.blender.org/wordpress/wp-content/
fi

echo
echo "==================================================================="
echo "Deploy to ${DEPLOYHOST} ${DRY_RUN_FLAG} is done."
echo "==================================================================="
